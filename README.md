# Software Engineering For The Cloud - PROJECT

The voting app project comes from https://github.com/BretFisher/example-voting-app

## Instruction

### Project stack

The voting app contains five microservices :
- vote : service to vote once either for cat or dog
- worker : service to process redis data to db
- redis : service of key-value storage to store vote on the frontend
- db : service to store vote on the backend
- result : service to show results

### Create a namespace
```sh
# The namespace will contain all the cluster resources 
kubectl create namespace voteapp
```

### Creating deployments
```sh
# Postgres DB with environment variable to bypass password requirement
kubectl create deployment db \
--image=postgres:9.4 \
--replicas=1 \
--port=5432 \
--namespace=voteapp
kubectl set env deployment/db POSTGRES_HOST_AUTH_METHOD=trust --namespace=voteapp

kubectl create deployment worker \
--image=bretfisher/examplevotingapp_worker \
--replicas=1 \
--namespace=voteapp

kubectl create deployment redis \
--image=redis:3.2 \
--replicas=1 \
--namespace=voteapp

# vote will contains 3 (can be more) replicas set
# The vote endpoint will be called by user, so if failure happen on one pod,
# while k8s is going to recover the pod, the user can access the service thanks to the other replicas 
kubectl create deployment vote \
--image=bretfisher/examplevotingapp_vote \
--replicas=3 \
--port=80 \
--namespace=voteapp

kubectl create deployment result \
--image=bretfisher/examplevotingapp_result \
--replicas=1 \
--port=80 \
--namespace=voteapp
```

### Creating services
```sh
# db and redis are service with a type of ClusterIP because their IP are internal to the cluster and shouldn't be access from outside
kubectl create service clusterip db --tcp=5432:5432 --namespace=voteapp
kubectl create service clusterip redis --tcp=6379:6379 --namespace=voteapp

# result will be accessible on port 31001 and vote on port 31000
# NodePort type allows to expose port to external IP
# we should pick high port number so no other process has been linked to that port 
kubectl create service nodeport result --tcp=5001:80 --node-port=31001 --namespace=voteapp
kubectl create service nodeport vote --tcp=5000:80 --node-port=31000 --namespace=voteapp

# We are not creating a service for worker because it doesn't expose any port
```

### Access application locally

- Go to localhost:31000 : vote for cat or dog
- Go to localhost:31001 : see answer in real-time

### Cleaning the cluster
```sh
# Delete deployments
kubectl delete \
deployment/db \
deployment/redis \
deployment/worker \
deployment/vote \
deployment/result \
--namespace=voteapp
# OR
kubectl delete --all deployment --namespace=voteapp

# Delete services
kubectl get service
service/db-dep \
service/redis \
service/worker \
service/vote \
service/result \
--namespace=voteapp
# OR
kubectl delete --all service --namespace=voteapp

# OR this will delete EVERYTHING
kubectl delete namespace voteapp

# Resources should be empty
kubectl get deployment --namespace=voteapp
kubectl get service --namespace=voteapp
# OR
kubectl get all --namespace=voteapp
```